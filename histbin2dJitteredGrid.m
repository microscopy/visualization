function [recImg,imgInfo]=histbin2dJitteredGrid(hObj,binwidth,xgrid,ygrid)
% histbin2dJitteredGrid([x,y])
%    2D histogram binning
%    uses default binwidth (pixel size) of 25
%
% optional input arguments
% histbin2dJitteredGrid([x,y],binwidth ,xgrid ,ygrid);
%    accumulates intensities according to 
%             xgrid and ygrid specified
%    binwidth specifies the bin size (pixel size)
%    binwidth can be empty (defaults to 25)
% histbin2dJitteredGrid(hObj ,binwidth ,xgrid ,ygrid);
%    uses SMLMdata object instead, and retrieves
%    (x,y) data from that.
%
% version 1.0
% (c) 2016
% Udo Birk

if(nargin < 2)
    binwidth = [];
end
if isempty(binwidth)
    binwidth = 25;
end

switch(class(hObj))
    case{'startOrte','SMLMdata'}
        x = hObj.getX();
        y = hObj.getY();

        M = [x y];
    otherwise
        M = double(hObj);
        if (ndims(M) ~= 2) | (min(size(M)) < 2)
            error('First input argument must be (x,y)-list of positions');
        end
        x = M(:,1);
        y = M(:,2);
end

%%
if(nargin < 3)
    xgrid = binwidth.*(round(min(x)./binwidth):1:round(max(x)./binwidth));
    ygrid = binwidth.*(round(min(y)./binwidth):1:round(max(y)./binwidth));
end
if(~isempty(xgrid))
    x = x - xgrid(1);
    xend   = round((xgrid(end)-xgrid(1))./binwidth)+1;
else
    xend   = 0;
end
if(~isempty(ygrid))
    y = y - ygrid(1);
    yend   = round((ygrid(end)-ygrid(1))./binwidth)+1;
else
    yend   = 0;
end

%%
xr = linspace(-binwidth/2,binwidth/2,11);
xr(1) = [];

if(xend*yend == 0)
    recImg = 0;
else
    recImg = 0;
    for xi = 1:length(xr);
        xc = xr(xi);
        for yi = 1:length(xr);
            yc = xr(yi);
            xPix = [round((x+xc)./binwidth)]+1;
            yPix = [round((y+yc)./binwidth)]+1;
            ind = (xPix > 0) & (xPix <= xend) & (yPix > 0) & (yPix <= yend);
            img = accumarray([[yPix(ind);yend] [xPix(ind);xend]],[ones(size(xPix(ind)));0]);
%            img = sparse(yl(ind),xl(ind),0.*x(ind)+1);
%            img = full(img);
            recImg = recImg+img;
        end
    end
    recImg = recImg./(numel(xr).^2);
end

%%
if(nargout > 1)
    imgInfo.xgrid = xgrid;
    imgInfo.ygrid = ygrid;
%    varargout{1}=imgInfo;
end

%%

